# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import pytest


class TestZsnlDatabasModule:
    def setup(self):
        pass

    def test_import_raises_not_implemented_error(self):
        with pytest.raises(NotImplementedError):
            import zsnl_database

            zsnl_database.raise_not_implemented_error()
