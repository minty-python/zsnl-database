# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


def raise_not_implemented_error():
    """
    This is a placeholder to raise a NotImplementedError on
    accidental import of deprected zsnl_database

    raise: NotImplementedError
    """
    raise NotImplementedError(
        f"This module as been moved to zsnl_domains.database starting from zsnl_domains@v0.6.78"
    )


raise_not_implemented_error()
