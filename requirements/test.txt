pytest==5.1.0
pytest-spec==2.0.0
pytest-cov==2.8.1
pytest-mock==1.13.0
safety

## Code style tools
black==v19.10b0
flake8==3.7.9
isort==4.3.10
safety
liccheck~=0.1
